<?php
    include 'conexion.php';
?>
<!doctype html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/all.css" />
    <link rel="stylesheet" href="css/estilo.css" />
    <title>Autos</title>
</head>

<body>
    <br />

    <nav>
        <div class="topnav">
            <a href="listar_autos.php">AUTOS</a>
        </div>
    </nav>
    <br />
    <div class="container">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-lg-10">
                        <h4>Lista de Autos</h4>
                    </div>
                    <div class="col-lg-2">
                        <a class="btn btn-success" href="agregar_auto.php"></i> Agregar</a>
                    </div>
                </div>
            </div>
            <?php
                $sql = "SELECT marcas.nombre as 'marca', autos.id, autos.nombre, autos.descripcion, autos.tipoCombustible, autos.cantidadPuertas, autos.precio FROM autos INNER JOIN marcas on marcas.id = autos.idMarca";             
            ?>

            <table class="table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Marca</th>
                        <th>Nombre</th>                       
                        <th>Descripcion</th>
                        <th>Combustible</th>
                        <th>Cantidad Puertas</th>
                        <th>Precio</th>
                        <th>Opciones</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach($conn->query($sql) as $row){
                        echo "<tr>";
                        echo "<td>" . $row["id"]. "</td>";
                        echo "<td>" . $row["marca"]. "</td>";
                        echo "<td>" . $row["nombre"]. "</td>";
                        echo "<td>" . $row["descripcion"]. "</td>";
                        echo "<td>" . $row["tipoCombustible"]. "</td>";
                        echo "<td>" . $row["cantidadPuertas"]. "</td>";
                        echo "<td>$ " . number_format($row["cantidadPuertas"],2). "</td>";
                        echo "<td>";
                        echo "<a class=\"btn btn-secondary\"  href=\"http://localhost/autos/ver_auto.php?codigo=". $row["id"]."\">Ver</a> ";
                        echo "<a class=\"btn btn-warning\"  href=\"http://localhost/autos/editar_auto.php?codigo=". $row["id"]."\">Editar</a> ";
                        echo "<a class=\"btn btn-danger\" href=\"http://localhost/autos/eliminar_auto.php?codigo=". $row["id"]."\">Eliminar</a>";
                        echo "</td>";
                        echo "</tr>";
                    }

                    ?>
                </tbody>
            </table>
        </div>
        <?php
            if (isset($_GET['result'])) {
               if($_GET['result'] == 1) {
                    echo "<div class=\"alert alert-success\" role=\"alert\">";
                    echo "Se ha eliminado el auto";
                    echo "</div>";
               }else{
                    echo "<div class=\"alert alert-danger\" role=\"alert\">";
                    echo "No se pudo eliminar el auto. ";                
                    echo "</div>";  
               }
            }
        ?>

    </div>

    <script src="js/jquery-3.5.1.slim.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>

</html>