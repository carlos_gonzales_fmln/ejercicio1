<?php
    include 'conexion.php';
?>
<!doctype html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/all.css" />
    <link rel="stylesheet" href="css/estilo.css" />
    <title>Autos</title>
</head>

<body>
    <br />

    <nav>
    </nav>
    <br />
    <div class="container">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-lg-10">
                        <h4>Ver Auto</h4>
                    </div>
                    <div class="col-lg-2">
                        <a class="btn btn-danger" href="listar_autos.php"></i> Volver</a>
                    </div>
                </div>
            </div>
            <?php
                $sql = "SELECT marcas.nombre as 'marca', autos.id, autos.nombre, autos.descripcion, autos.tipoCombustible, autos.cantidadPuertas, autos.precio FROM autos INNER JOIN marcas on marcas.id = autos.idMarca WHERE autos.id = '".$_GET['codigo']."'";             
            ?>

            <table class="table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Marca</th>
                        <th>Nombre</th>                       
                        <th>Descripcion</th>
                        <th>Combustible</th>
                        <th>Cantidad Puertas</th>
                        <th>Precio</th>
                        <th>Opciones</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach($conn->query($sql) as $row){
                        echo "<tr>";
                        echo "<td>" . $row["id"]. "</td>";
                        echo "<td>" . $row["marca"]. "</td>";
                        echo "<td>" . $row["nombre"]. "</td>";
                        echo "<td>" . $row["descripcion"]. "</td>";
                        echo "<td>" . $row["tipoCombustible"]. "</td>";
                        echo "<td>" . $row["cantidadPuertas"]. "</td>";
                        echo "<td>$ " . number_format($row["cantidadPuertas"],2). "</td>";
                        echo "</tr>";
                    }

                    ?>
                </tbody>
            </table>
        </div>
    </div>
    <script src="js/jquery-3.5.1.slim.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>

</html>